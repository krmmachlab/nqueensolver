﻿using UnityEngine;

namespace NQueen
{
    /// <summary>
    /// The QueenSelectHandler class handles the actions of selecting a queen on the chess board.
    /// The following rules apply:
    /// <list type="bullet">
    /// <item> If a queen is selected, all the tiles it can attack are also selected (including the tile under it). </item>
    /// <item> When a new queen is selected, the previous one (and its attacked tiles) is deselected. </item>
    /// <item> If the same queen is selected, it becomes deselected. </item>
    /// </list>
    /// </summary>
    public class QueenSelectHandler : MonoBehaviour
    {
        /// <summary>Called when this queen is clicked on using the left mouse button.</summary>
        void OnMouseDown()
        {
            ActivateQueen();
        }

        /// <summary>Activates or deactivates this queen.</summary>
        /// <param name="_activate">  If set to <c>true</c>, this queen is activated. Else it is deactivated.</param>
        private void ActivateQueen(bool _activate = true)
        {
            Vector2Int pos = DataHolder.GetInstance().GOToPos[transform.parent.gameObject];
            if (_activate)
            {
                bool selectedNewQueen = UpdateSelectedQueen();
                if (!selectedNewQueen) return;

                // Set the queen's color based on the color of the tile it stands on.
                if (DataHolder.GetInstance().BlackHashset.Contains(DataHolder.GetInstance().posToGO[pos]))
                    GetComponent<Renderer>().material.color = Constants.SELECT_COLOR_BLACK;
                else GetComponent<Renderer>().material.color = Constants.SELECT_COLOR_WHITE;
            }
            // If we're deactivating, set the color back to the default color.
            else GetComponent<Renderer>().material.color = Constants.DEFAULT_COLOR_QUEEN;

            ActivateAttackedTiles(pos, _activate);
        }

        /// <summary>Updates the selected queen in the DataHolder and deactivated the previous selected queen.</summary>
        /// <returns>
        ///   <para>Returns true if a new queen is selected, and returns false is the same queen is selected.</para>
        /// </returns>
        private bool UpdateSelectedQueen()
        {
            if (DataHolder.GetInstance().selectedQueen != null)
            {
                // Deactivate previous queen (and its attacked tiles).
                DataHolder.GetInstance().selectedQueen.GetComponent<QueenSelectHandler>().ActivateQueen(false);
            }
            if (DataHolder.GetInstance().selectedQueen == gameObject)
            {
                DataHolder.GetInstance().selectedQueen = null;
                return false;
            }
            DataHolder.GetInstance().selectedQueen = gameObject;
            return true;
        }

        /// <summary>Activates or deactivated all attacked tiles from the currently selected queen.</summary>
        /// <param name="_pos">The chess board position of the selected queen.</param>
        /// <param name="_activate">  If set to <c>true</c>, the tiles are activated. Else they are deactivated.</param>
        private void ActivateAttackedTiles(Vector2Int _pos, bool _activate)
        {

            for (int i = 1; i <= DataHolder.GetInstance().N; i++)
            {
                // There are 4 lines (horizontal, vertical, and two diagonals) with possible positions.
                Vector2Int tilePos1 = new Vector2Int(_pos.x, i);
                Vector2Int tilePos2 = new Vector2Int(i, _pos.y);
                Vector2Int tilePos3 = new Vector2Int((_pos.x - _pos.y) + i, i);
                Vector2Int tilePos4 = new Vector2Int(i, (_pos.y + _pos.x) - i);
                ActivateTile(tilePos1, _activate);
                ActivateTile(tilePos2, _activate);
                ActivateTile(tilePos3, _activate);
                ActivateTile(tilePos4, _activate);
            }
        }

        /// <summary>Activates or deactivates a single tile at a certain position by changing its color.</summary>
        /// <param name="_pos">The chess board position of this tile.</param>
        /// <param name="_activate">  If set to <c>true</c>, this tile is activated. Else it is deactivated.</param>
        private void ActivateTile(Vector2Int _pos, bool _activate)
        {
            if (DataHolder.GetInstance().posToGO.ContainsKey(_pos))
            {
                GameObject tile = DataHolder.GetInstance().posToGO[_pos];
                if (DataHolder.GetInstance().BlackHashset.Contains(tile))
                {
                    // Activate this black tile.
                    if (_activate) tile.GetComponent<Renderer>().material.color = Constants.SELECT_COLOR_BLACK;
                    else tile.GetComponent<Renderer>().material.color = Constants.DEFAULT_COLOR_BLACK;
                }
                else
                {
                    // Activate this white tile.
                    if (_activate) tile.GetComponent<Renderer>().material.color = Constants.SELECT_COLOR_WHITE;
                    else tile.GetComponent<Renderer>().material.color = Constants.DEFAULT_COLOR_WHITE;
                }
            }
        }
    }
}
