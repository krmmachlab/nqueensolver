﻿using UnityEngine;

namespace NQueen
{
    /// <summary>
    /// The BoardGenerator class generates the chess board, given N and the queen sequence.
    /// </summary>
    public class BoardGenerator : MonoBehaviour
    {
        /// <summary>Start is called before the first frame update (MonoBehaviour method). Generates the chess board. </summary>
        void Start()
        {
            GenerateBoard(DataHolder.GetInstance().N, DataHolder.GetInstance().NQueenSolver.GetSequence());
        }

        /// <summary>Generates a label (3D text) at a specific index on the row or column of the chess board.</summary>
        /// <param name="_xz">The x or z value. If this is a row label, _xz denotes the x value, otherwise it denotes the z value.</param>
        /// <param name="_index">The index of the row/column.</param>
        /// <param name="_rowLabel">  If set to <c>true</c>, this label represents a row label. Else it represents a column label.</param>
        private void GenerateLabel(float _xz, int _index, bool _rowLabel)
        {
            GameObject label = new GameObject("label " + _index);
            if (_rowLabel)
            {
                // These offsets are obtained through trial and error. They ensure that the number is always centered.
                float offset = _index > 8 ? (_index > 98 ? 4f : 2.5f) : 1;
                label = Instantiate(Resources.Load(Constants.PREFAB_LABEL), new Vector3(_xz - offset, 0, -1), Quaternion.Euler(new Vector3(90, 0, 0))) as GameObject;
            }
            else
            {
                // These offsets are obtained through trial and error. They ensure that the number is always centered.
                float offset = _index > 8 ? (_index > 98 ? 5f : 2.5f) : 0;
                label = Instantiate(Resources.Load(Constants.PREFAB_LABEL), new Vector3(-5 - offset, 0, _xz + 2.5f), Quaternion.Euler(new Vector3(90, 0, 0))) as GameObject;
            }
            TextMesh textMesh = label.GetComponent<TextMesh>();
            textMesh.text = "" + (_index + 1);
            textMesh.fontSize = Constants.CHESS_BOARD_LABEL_FONT_SIZE;
            textMesh.color = Color.green;
        }

        /// <summary>Instantiates a chess board tile (and a queen on it if a queen should be placed there).</summary>
        /// <param name="_position">The transform position at which the tile should be instantiated.</param>
        /// <param name="_row">The row on the chess board.</param>
        /// <param name="_column">The column on the chess board.</param>
        /// <param name="_isBlack">If set to <c>true</c>, this tile represents a black tile. Else it represents a white tile.</param>
        /// <param name="_queenSequence">The queen sequence.</param>
        private void InstantiateAt(Vector3 _position, int _row, int _column, bool _isBlack, int[] _queenSequence)
        {
            GameObject tile = new GameObject();
            // Instantiate black tile.
            if (_isBlack)
            {
                tile = Instantiate(Resources.Load(Constants.PREFAB_TILE_BLACK), _position, Quaternion.identity) as GameObject;
                DataHolder.GetInstance().BlackHashset.Add(tile);
            }
            // Instantiate white tile.
            else tile = Instantiate(Resources.Load(Constants.PREFAB_TILE_WHITE), _position, Quaternion.identity) as GameObject;

            DataHolder.GetInstance().posToGO.Add(new Vector2Int(_row + 1, _column + 1), tile);

            // If there is a queen at this position, instantiate the queen GameObject.
            if (_queenSequence[_row] == _column + 1)
            {
                Vector3 queenPosition = new Vector3(_position.x, 0.1f, _position.z);
                GameObject queenGO = Instantiate(Resources.Load(Constants.PREFAB_QUEEN), queenPosition, Quaternion.identity) as GameObject;
                DataHolder.GetInstance().GOToPos.Add(queenGO, new Vector2Int(_row + 1, _column + 1));
            }
        }

        /// <summary>Generates the chess board: Tiles, queens and row/column labels.</summary>
        /// <param name="_n">The number of queens.</param>
        /// <param name="_queenSequence">The queen sequence.</param>
        private void GenerateBoard(int _n, int[] _queenSequence)
        {
            bool isBlack = true;
            for (int i = 0; i < _n; i++)
            {
                int x = 5 + (i * 10);
                GenerateLabel(x, i, true);
                for (int j = 0; j < _n; j++)
                {
                    int z = 5 + (j * 10);
                    Vector3 position = new Vector3(x, 0, z);
                    if (i == 0) GenerateLabel(z, j, false);
                    InstantiateAt(position, i, j, isBlack, _queenSequence);
                    isBlack = !isBlack;
                }
                if (_n % 2 == 0) isBlack = !isBlack;
            }
        }
    }
}