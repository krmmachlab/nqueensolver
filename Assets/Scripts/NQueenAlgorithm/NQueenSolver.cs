﻿using System.Threading;
using UnityEngine;

namespace NQueen
{
    /// <summary>NQueenSolver is the abstract class for solving the N-Queen problem.
    /// It uses multithreading, and both its inherited classes are multithreaded solvers.</summary>
    public abstract class NQueenSolver
    {
        /// <summary>The number of queens.</summary>
        protected int N;
        /// <summary>The queen sequence AKA the solution to the problem. The index/element represent the row/column.</summary>
        protected int[] queenSequence;
        /// <summary>A boolean that is set to true once a solution has been found.</summary>
        protected bool solutionFound;
        /// <summary>A Mutex used to lock specific areas that update shared data among all threads.</summary>
        protected Mutex mutex;

        /// <summary>Initializes a new instance of the <see cref="NQueenSolver"/> class.</summary>
        protected NQueenSolver()
        {
            N = 4;
            queenSequence = new int[N];
            solutionFound = false;
            mutex = new Mutex();
        }

        /// <summary>Solves the N-Queen problem for the specified N. It creates N threads,
        /// where each is tasked with the same method but a different parameter from 1 to N. </summary>
        /// <param name="_n">  The number of queens.</param>
        public void Solve(int _n)
        {
            N = _n;
            queenSequence = new int[N];
            for (int i = 1; i <= N; i++)
            {
                int q = i;
                Thread childThread = new Thread(() => ThreadExecute(q));
                childThread.Start();
            }
        }

        /// <summary>Each thread executes this method to solve its part of the N-Queen problem.</summary>
        /// <param name="_q">
        ///  A specific int from 1 to N. Usage depends on the implementation. For the greedy solver,
        ///  it represents the column at which all queens start. For the backtracking solver,
        ///  it represents the column at which the first queen (row 1) is placed.
        /// </param>
        protected abstract void ThreadExecute(int _q);

        /// <summary>Ends the solver.
        /// It is called when one thread finds a solution.</summary>
        /// <param name="_endSequence">The winning sequence AKA solution to the problem.</param>
        protected void EndSolver(int[] _endSequence)
        {
            mutex.WaitOne();
            if (!solutionFound)
            {
                solutionFound = true;
                queenSequence = _endSequence;
            }
            mutex.ReleaseMutex();
        }

        /// <summary>Checks and returns the ratio of completion.</summary>
        /// <returns>The ratio of completion.</returns>
        public abstract float CheckCompletion();

        /// <summary>Gets the queen sequence.</summary>
        /// <returns>The queen sequence AKA solution.</returns>
        public int[] GetSequence()
        {
            return queenSequence;
        }

        /// <summary>Prints the queen sequence AKA solution.</summary>
        public void PrintSequence()
        {
            string str = "";
            foreach (int s in queenSequence)
            {
                str += (s + ",");
            }
            str = str.Remove(str.Length - 1);
            Debug.Log(str);
        }
    }
}