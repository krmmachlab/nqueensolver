﻿using System;
using System.Collections.Generic;

namespace NQueen
{
    /// <summary>
    /// A multithreaded greedy N-Queen solver with a polynomial complexity (C.N^3 to C.N^2). It is also a Singleton.
    /// The algorithm goes as follows:
    /// <list type="number">
    /// <item> For each thread from 1 to N, all queens are initialized on the same column at column q (from 1 to N). </item>
    /// <item> For each iteration from 1 to Max, we go through all queens and move each one to the spot in its
    /// row where it is attacked the least. If there are multiple spots with the least attacks, the solver
    /// either chooses the first one, or one of them at random (the random method is used in the final solution
    /// and works best). </item>
    /// <item> After each iteration we check if we have a solution, else we continue with the next iteration. </item>
    /// </list>
    /// </summary>
    /// <seealso cref="NQueen.NQueenSolver" />
    public class NQueenSolver_Greedy : NQueenSolver
    {
        /// <summary>The only instance
        /// of this Singleton class.</summary>
        private static NQueenSolver_Greedy instance;

        /// <summary>The number of queen updates
        /// performed so far.</summary>
        private int queenUpdates;

        /// <summary>The private constructor for the <see cref="NQueenSolver_Greedy"/> class.</summary>
        private NQueenSolver_Greedy() : base()
        {
            queenUpdates = 0;
        }

        /// <summary>Gets the instance.</summary>
        /// <returns>The instance of this Singleton class.</returns>
        public static NQueenSolver GetInstance()
        {
            return instance ?? (instance = new NQueenSolver_Greedy());
        }

        /// <summary>Each thread executes this method to solve its part of the N-Queen problem.</summary>
        /// <param name="_q">
        ///  A specific int from 1 to N. It represents the column at which all queens start.
        /// </param>
        protected override void ThreadExecute(int _q)
        {
            int[] queenSequence_Thread = new int[N];
            for (int i = 0; i < N; i++)
            {
                queenSequence_Thread[i] = _q;
            }
            NQueens_Step(queenSequence_Thread);
        }

        /// <summary>Safely increments the queen updates (shared by all threads).</summary>
        private void IncrementQueenUpdates()
        {
            mutex.WaitOne();
            if (!solutionFound) queenUpdates++;
            mutex.ReleaseMutex();
        }

        /// <summary>The main algorithm of the greedy solver. It loops over all iterations and tries to
        /// solve it by updating all queens to new position in their rows (by choosing the least
        /// attacked spot). If a solution is found, it ends the solver and all other threads terminate.</summary>
        /// /// <param name="_queenSequence_Thread">The thread-specific queen sequence.</param>
        private void NQueens_Step(int[] _queenSequence_Thread)
        {
            for (int i = 0; i < Constants.GREEDY_SOLVER_MAX_ITERATIONS; i++)
            {
                if (solutionFound) return;
                if (NoneAttacking(_queenSequence_Thread))
                {
                    EndSolver(_queenSequence_Thread);
                    return;
                }
                else
                {
                    NQueens_Iteration(ref _queenSequence_Thread);
                }
            }
        }

        /// <summary>A single iteration of the greedy solver. It loops through all queens and changes
        /// their spots within the same row based on the least attacked spot.</summary>
        /// <param name="_queenSequence_Thread">A reference to the thread-specific queen sequence.</param>
        private void NQueens_Iteration(ref int[] _queenSequence_Thread)
        {
            for (int row = 1; row <= N; row++)
            {
                if (solutionFound) return;
                _queenSequence_Thread[row - 1] = MinAttacksPosition_Random(row, _queenSequence_Thread);
                IncrementQueenUpdates();
            }
        }

        /// <summary>Checks if any queen is attacked, i.e. if a solution is found.</summary>
        /// <param name="_queenSequence_Thread">The thread-specific queen sequence.</param>
        /// <returns>Returns true if we have a solution (no queen is attacked). Otherwise, returns false.</returns>
        private bool NoneAttacking(int[] _queenSequence_Thread)
        {
            bool valid = true;
            for (int row = 1; row <= N; row++)
            {
                if (CalculateAttacks(row, _queenSequence_Thread[row - 1], _queenSequence_Thread) != 0)
                {
                    valid = false;
                    break;
                }
            }
            return valid;
        }

        /// <summary>
        /// Returns the minimum attacked position in a row for a specific queen at row _row.
        /// This method chooses the first spot of the least attacked spots. This method is not used in the final solver.
        /// </summary>
        /// <param name="_row">The row of the queen.</param>
        /// <param name="_queenSequence_Thread">The thread-specific queen sequence.</param>
        /// <returns>The first column at which the queen is least attacked.</returns>
        private int MinAttacksPosition(int _row, int[] _queenSequence_Thread)
        {
            int minAttacks = N;
            int bestColumnPos = _queenSequence_Thread[_row - 1];
            for (int column = 1; column <= N; column++)
            {
                int attacks = CalculateAttacks(_row, column, _queenSequence_Thread);
                if (attacks < minAttacks)
                {
                    minAttacks = attacks;
                    bestColumnPos = column;
                }
            }
            return bestColumnPos;
        }

        /// <summary>
        /// Returns the minimum attacked position in a row for a specific queen at row _row.
        /// This method chooses one of the least attacked spots at random.
        /// </summary>
        /// <param name="_row">The row of the queen.</param>
        /// <param name="_queenSequence_Thread">The thread-specific queen sequence.</param>
        /// <returns>One of the columns (at random) where the queen is least attacked.</returns>
        private int MinAttacksPosition_Random(int _row, int[] _queenSequence_Thread)
        {
            int minAttacks = N;
            int[] attacksArray = new int[N];
            for (int column = 1; column <= N; column++)
            {
                int attacks = CalculateAttacks(_row, column, _queenSequence_Thread);
                if (attacks < minAttacks)
                {
                    minAttacks = attacks;
                }
                attacksArray[column - 1] = attacks;
            }
            List<int> candidateColumns = new List<int>();
            for (int column = 1; column <= N; column++)
            {
                if (attacksArray[column - 1] == minAttacks) candidateColumns.Add(column);
            }
            var random = new System.Random();
            int randomIndex = random.Next(candidateColumns.Count);
            return candidateColumns[randomIndex];
        }

        /// <summary>Calculates the number of attacks for a queen at a certain position (by how many queens it is attacked by).</summary>
        /// <param name="_row">The row of the queen.</param>
        /// <param name="_column">The column of the queen.</param>
        /// <param name="_queenSequence_Thread">The thread-specific queen sequence.</param>
        /// <returns></returns>
        private int CalculateAttacks(int _row, int _column, int[] _queenSequence_Thread)
        {
            int attacks = 0;
            for (int row2 = 1; row2 <= N; row2++)
            {
                if (row2 == _row) continue;
                if (IsAttacking(_row, _column, row2, _queenSequence_Thread))
                {
                    attacks++;
                }
            }
            return attacks;
        }

        /// <summary>Determines whether a queen at row1, column1 is attacking the queen at row2.</summary>
        /// <param name="_row1">The row of queen 1.</param>
        /// <param name="_column1">The column of queen 1.</param>
        /// <param name="_row2">The row of queen 2.</param>
        /// <param name="_queenSequence_Thread">The thread-specific queen sequence.</param>
        /// <returns><c>true</c> if queen 1 is attacking queen 2; otherwise, <c>false</c>.</returns>
        private bool IsAttacking(int _row1, int _column1, int _row2, int[] _queenSequence_Thread)
        {
            if (_queenSequence_Thread[_row2 - 1] == _column1 || (Math.Abs(_queenSequence_Thread[_row2 - 1] - _column1) == Math.Abs(_row2 - _row1)))
            {
                return true;
            }
            return false;
        }

        /// <summary>Checks and returns the ratio of completion, based on how many queen updates are done so far.</summary>
        /// <returns>The ratio of completion.</returns>
        public override float CheckCompletion()
        {
            if (solutionFound) return 1f;
            // The average number of iterations is used to calculate the progress upper bound.
            float progress = (1f * queenUpdates) / (N * N * Constants.GREEDY_SOLVER_AVG_ITERATIONS);
            if (progress > 1f) progress = 1f;
            return progress;
        }
    }
}