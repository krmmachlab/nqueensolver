﻿using System;
using System.Numerics;

namespace NQueen
{
    /// <summary>
    /// A multithreaded N-Queen solver that uses backtracking. It is also a Singleton. The algorithm essentially tries
    /// every possible valid placement/permutation in a recursive manner. It's a very slow algorithm with
    /// exponential complexity, and therefore it is not used in the final solution of this project. It goes as follows:
    /// <list type="number">
    /// <item> For each thread from 1 to N, the first queen is initialized at column q (from 1 to N). </item>
    /// <item> The solver tries to place each queen (after the first) on a valid (non-attacked) spot in the same row,
    /// and recursively repeats this process for the queens that follow. If the solver reaches an
    /// invalid situation, it backtracks to the previous positioning.</item>
    /// </list>
    /// </summary>
    /// <seealso cref="NQueen.NQueenSolver" />
    public class NQueenSolver_Backtracking : NQueenSolver
    {
        /// <summary>The only instance
        /// of this Singleton class.</summary>
        private static NQueenSolver_Backtracking instance;

        /// <summary>The number of explored nodes
        /// so far. A BigInteger is used because this number can get gigantic.</summary>
        private BigInteger exploredNodes;
        /// <summary>The total number of possible nodes. A BigInteger is used because this number can get gigantic.</summary>
        private BigInteger totalNodes;

        /// <summary>The private constructor for the <see cref="NQueenSolver_Backtracking"/> class.</summary>
        private NQueenSolver_Backtracking() : base()
        {
            exploredNodes = 1;
            totalNodes = MaxNrNodes(N);
        }

        /// <summary>Gets the instance.</summary>
        /// <returns>The instance of this Singleton class.</returns>
        public static NQueenSolver GetInstance()
        {
            return instance ?? (instance = new NQueenSolver_Backtracking());
        }

        /// <summary>Each thread executes this method to solve its part of the N-Queen problem.</summary>
        /// <param name="_q">
        ///  A specific int from 1 to N. It represents the column at which the first queen (row 1) is placed
        /// </param>
        protected override void ThreadExecute(int _q)
        {
            int[] queenSequence_Thread = new int[N];
            queenSequence_Thread[0] = _q;
            IncrementExploredNodes(1);
            NQueens_Step(2, queenSequence_Thread);
        }

        /// <summary>Safely increments the number of explored nodes (shared by all threads).</summary>
        private void IncrementExploredNodes(BigInteger _inc)
        {
            mutex.WaitOne();
            if (!solutionFound) exploredNodes += _inc;
            mutex.ReleaseMutex();
        }

        /// <summary> 
        /// The recursive method that solves the N-Queen problem.
        /// The queen at the given row is moved to different spots (columns) on the same row.
        /// If it's a valid spot (i.e. not attacked) we continue recursively with the next row.
        /// If a solution is found we stop the solver. All other threads terminate as a result.
        /// Note that if a route is invalid, the number of explored nodes is incremented based on
        /// how many child nodes there are (that don't need to be explored because the parent is invalid).
        /// </summary>
        /// <param name="_row">The row or current queen (there's always only 1 queen on a row).</param>
        /// <param name="_queenSequence_Thread">The thread-specific queen sequence.</param>
        private void NQueens_Step(int _row, int[] _queenSequence_Thread)
        {
            for (int column = 1; column <= N; column++)
            {
                if (IsValid(_row, column, _queenSequence_Thread))
                {
                    IncrementExploredNodes(1);
                    _queenSequence_Thread[_row - 1] = column;
                    if (_row == N)
                    {
                        // FOUND A SOLUTION
                        EndSolver(_queenSequence_Thread);
                        return;
                    }
                    else
                    {
                        NQueens_Step(_row + 1, _queenSequence_Thread);
                        if (solutionFound) return;
                    }
                }
                else
                {
                    // Increment by how many child nodes we don't need to explore anymore.
                    IncrementExploredNodes(MaxNrNodes(N - _row));
                }
            }
        }

        /// <summary>Returns true if the given queen placement is valid (not attacked).</summary>
        /// <param name="_row">The row of the queen.</param>
        /// <param name="_column">The column of the queen.</param>
        /// <param name="_queenSequence_Thread">The thread-specific queen sequence.</param>
        /// <returns>
        ///   <c>true</c> if the specified queen placement is valid (not attacked); otherwise, <c>false</c>.</returns>
        private bool IsValid(int _row, int _column, int[] _queenSequence_Thread)
        {
            for (int row2 = 1; row2 <= _row - 1; row2++)
            {
                if (IsAttacking(_row, _column, row2, _queenSequence_Thread)) return false;
            }
            return true;
        }

        /// <summary>Determines whether a queen at row1, column1 is attacking the queen at row2.</summary>
        /// <param name="_row1">The row of queen 1.</param>
        /// <param name="_column1">The column of queen 1.</param>
        /// <param name="_row2">The row of queen 2.</param>
        /// <param name="_queenSequence_Thread">The thread-specific queen sequence.</param>
        /// <returns><c>true</c> if queen 1 is attacking queen 2; otherwise, <c>false</c>.</returns>
        private bool IsAttacking(int _row1, int _column1, int _row2, int[] _queenSequence_Thread)
        {
            if (_queenSequence_Thread[_row2 - 1] == _column1 || (Math.Abs(_queenSequence_Thread[_row2 - 1] - _column1) == Math.Abs(_row2 - _row1)))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Returns the number of child nodes that could be explored (all possible permutations) for a specific N. This method is used to keep track of the explored nodes to calculate the overall progress of the solver.
        /// </summary>
        /// <param name="_n">The number of queens (at a specific level).</param>
        /// <returns>The number of child nodes that could be explored.</returns>
        private BigInteger MaxNrNodes(int _n)
        {
            BigInteger sum = 1;
            for (int i = 0; i < _n; i++)
            {
                BigInteger prod = 1;
                for (int j = 0; j <= i; j++)
                {
                    prod *= (_n - j);
                }
                sum += prod;
            }
            return sum;
        }

        /// <summary>Checks and returns the ratio of completion, based on how many nodes in the tree are explored.</summary>
        /// <returns>The ratio of completion.</returns>
        public override float CheckCompletion()
        {
            if (solutionFound) return 1f;
            else return (float)BigInteger.Divide(exploredNodes, totalNodes);
        }
    }
}