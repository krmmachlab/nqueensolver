﻿using UnityEngine;

namespace NQueen
{
    /// <summary>
    ///   <para>
    ///     This project implements an N-Queen solver. It starts with a menu scene where the user can input a
    ///     value for N. The user can then click a button to start the solver and generate the chess board.
    ///     As the solver is working to find a solution, a loading bar is shown with the current progress or
    ///     percentage of completion. Once the solution is found, a new scene is loaded with a generated chess
    ///     board that displays the solution. The solver time is shown in the UI, among other useful information.
    ///     The user can move the camera around and zoom in/out to see things clearly (if N is large).
    ///     The user can also select a queen to see all tiles it attacks (to make sure the solution is correct).
    ///   </para>  
    ///   <para>
    ///     There are 2 implemented algorithms for the N-Queen problem. The first one is a backtracking algorithm
    ///     that is very slow (exponential complexity), and the second one is a greedy algorithm that is way faster
    ///     (C.N^3 to C.N^2 complexity). The greedy algorithm is used as the final solution (but you can easily
    ///     try the backtracking algorithm by uncommenting line 66 in the DataHolder class and commenting line 67).
    ///     Note that the backtracking algorithm is not a valid choice for N>25 (takes too long), and the progress
    ///     bar can return invalid completion ratios since the values for all possible tree nodes and explored nodes
    ///     exceed the limits of even BigInteger. Both algorithms are multithreaded are run on separate threads
    ///     that don't block the UI. The two algorithms are explained in the documentation of their corresponding classes.
    ///   </para>
    ///   <para>
    ///     Since the greedy solution is so fast, seeing the loading bar's progression will be hard for small N,
    ///     therefore try values for N>100 to see the loading bar in action. It is also unknown when the solver will
    ///     find a solution, so the progress bar might suddenly jump to 100%. An average number of 5 iterations
    ///     is used for the loading bar (greedy solver), since in almost all cases less than 5 iterations
    ///     per thread are done. This ensures that the loading bar looks good, with a smooth progression towards 100%.
    ///   </para>
    ///   <para>
    ///     A UML class diagram is also available in the visual studio project (ClassDiagram1.cd).
    ///   </para>
    /// </summary>
    internal class NamespaceDoc
    {
    }

    /// <summary>A static class that holds different constant values used throughout the project.</summary>
    public static class Constants
    {
        /// <summary>The base camera movement.</summary>
        public static float BASE_CAMERA_MOVEMENT = 0.05f;

        /// <summary>The minimum allowed N.</summary>
        public static int N_MIN = 4;
        /// <summary>The maximum allowed N.</summary>
        public static int N_MAX = 250;

        /// <summary>The maximum number of iterations for the greedy solver.</summary>
        public static int GREEDY_SOLVER_MAX_ITERATIONS = 1000;

        /// <summary>
        /// The average number of iterations for the greedy solver.
        /// This number was obtained through trial and error. Typically most solving
        /// times ended before 5 iterations per thread, with a few exceptions like for N=6.
        /// </summary>
        public static int GREEDY_SOLVER_AVG_ITERATIONS = 5;

        /// <summary>The size of a chess tile.</summary>
        public static float TILE_SIZE = 10f;
        /// <summary>Camera padding for x/z values (used for setting camera bounds).</summary>
        public static float CAMERA_PADDING_XZ = 20f;
        /// <summary>A factor of N used to indicate how far the camera should be from the board.</summary>
        public static float CAMERA_FACTOR_Y = 10f;

        /// <summary>The chess board label font size.</summary>
        public static int CHESS_BOARD_LABEL_FONT_SIZE = 50;

        /// <summary>The default color of a queen.</summary>
        public static Color DEFAULT_COLOR_QUEEN = new Color(0.67f, 0.104f, 0.104f, 1f);
        /// <summary>The default color of black tile.</summary>
        public static Color DEFAULT_COLOR_BLACK = Color.black;
        /// <summary>The default color of white tile.</summary>
        public static Color DEFAULT_COLOR_WHITE = Color.white;

        /// <summary>The color of black tile when it is selected.</summary>
        public static Color SELECT_COLOR_BLACK = Color.green;
        /// <summary>The color of white tile when it is selected.</summary>
        public static Color SELECT_COLOR_WHITE = Color.yellow;

        ///////////////////////////////////////// CONTROLS /////////////////////////////////////////

        // UP
        /// <summary>The first control for Up.</summary>
        public static string CONTROLS_UP1 = "w";
        /// <summary>The second control for Up.</summary>
        public static string CONTROLS_UP2 = "up";

        // DOWN
        /// <summary>The first control for Down.</summary>
        public static string CONTROLS_DOWN1 = "s";
        /// <summary>The second control for Down.</summary>
        public static string CONTROLS_DOWN2 = "down";

        // LEFT
        /// <summary>The first control for Left.</summary>
        public static string CONTROLS_LEFT1 = "a";
        /// <summary>The second control for Left.</summary>
        public static string CONTROLS_LEFT2 = "left";

        // RIGHT
        /// <summary>The first control for Right.</summary>
        public static string CONTROLS_RIGHT1 = "d";
        /// <summary>The second control for Right.</summary>
        public static string CONTROLS_RIGHT2 = "right";

        // ZOOM IN
        /// <summary>The control for Zooming In.</summary>
        public static string CONTROLS_ZOOMIN = "q";

        // ZOOM OUT
        /// <summary>The control for Zooming Out.</summary>
        public static string CONTROLS_ZOOMOUT = "e";

        ///////////////////////////////////////// PREFABS /////////////////////////////////////////

        /// <summary>The default Prefabs path.</summary>
        public static string PREFAB_PATH = "Prefabs/";
        /// <summary>The Prefab path for a chess board label (3D text).</summary>
        public static string PREFAB_LABEL = PREFAB_PATH + "ChessBoardLabel";
        /// <summary>The Prefab path for a black tile.</summary>
        public static string PREFAB_TILE_BLACK = PREFAB_PATH + "ChessTile_Black";
        /// <summary>The Prefab path for a white tile.</summary>
        public static string PREFAB_TILE_WHITE = PREFAB_PATH + "ChessTile_White";
        /// <summary>The Prefab path for a queen.</summary>
        public static string PREFAB_QUEEN = PREFAB_PATH + "ChessQueen";
    }
}