﻿using UnityEngine;using System.Collections.Generic;

namespace NQueen
{
    /// <summary>
    /// The DataHolder class is a Singleton that holds data used throughout the solver (in both scenes).
    /// It holds the following data:
    /// <list type="bullet">
    /// <item> N. </item>
    /// <item> The N-Queen solver (includes the solution sequence). </item>
    /// <item> The time taken to solve this N-Queen problem. </item>
    /// <item> The selected queen GameObject. </item>
    /// <item> A Dictionary that returns a tile GameObject from a chess board position. </item>
    /// <item> A Dictionary that returns a chess board position from a GameObject. </item>
    /// <item> A Hashset that includes all GameObjects representing a black tile. </item>
    /// </list>
    /// </summary>
    public class DataHolder : MonoBehaviour
    {
        /// <summary>The only instance
        /// of this Singleton class.</summary>
        private static DataHolder instance;

        /// <summary>The number of queens.</summary>
        public int N;
        /// <summary>The N-Queen solver.</summary>
        public NQueenSolver NQueenSolver;
        /// <summary>The solver time in milliseconds.</summary>
        public float solverTime;

        /// <summary>The selected queen GameObject.</summary>
        public GameObject selectedQueen;
        /// <summary>A Dictionary that returns a tile GameObject from a chess board position key.</summary>
        public Dictionary<Vector2Int, GameObject> posToGO;
        /// <summary>A Dictionary that returns a chess board position from a queen GameObject key.</summary>
        public Dictionary<GameObject, Vector2Int> GOToPos;
        /// <summary>A Hashset that includes all GameObjects representing a black tile.</summary>
        public HashSet<GameObject> BlackHashset;


        /// <summary>Returns the only instance of this Singleton class.</summary>
        /// <returns>DataHolder instance.</returns>
        public static DataHolder GetInstance()
        {
            return instance;
        }

        /// <summary>Start is called before the first frame update (MonoBehaviour method). Initializes the data.</summary>
        void Start()
        {
            InitializeData();
        }

        /// <summary>Initializes the data used throughout the solver (in both scenes).</summary>
        private void InitializeData()
        {
            instance = this;
            posToGO = new Dictionary<Vector2Int, GameObject>();
            GOToPos = new Dictionary<GameObject, Vector2Int>();
            BlackHashset = new HashSet<GameObject>();
            N = Constants.N_MIN;
            solverTime = 0f;

            // Choose a solver. The greedy algorithm is way faster.

            //NQueenSolver = NQueenSolver_Backtracking.GetInstance();
            NQueenSolver = NQueenSolver_Greedy.GetInstance();
        }
    }
}