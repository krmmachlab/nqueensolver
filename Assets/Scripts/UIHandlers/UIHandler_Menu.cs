﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace NQueen
{
    /// <summary>
    /// The UIHandler_Menu class handles the UI for the menu scene.
    /// </summary>
    public class UIHandler_Menu : MonoBehaviour
    {
        /// <summary>The start button that starts the solver.</summary>
        public Button startButton;
        /// <summary>The input field to enter a value for N.</summary>
        public InputField inputField;
        /// <summary>The loading bar.</summary>
        public Slider loadingBar;
        /// <summary>The text in the UI that displays the percentage of completion for the loading bar.</summary>
        public Text percentageText;

        /// <summary>The stopwatch used to measure the solver time.</summary>
        private Stopwatch stopwatch;
        /// <summary>A boolean that shows if the entered value for N is valid.</summary>
        private bool validN = true;

        /// <summary>Start is called before the first frame update (MonoBehaviour method). Initializes the UI for the menu scene.</summary>
        void Start()
        {
            InitializeUI();
        }

        /// <summary>Initializes the UI for the menu scene.</summary>
        private void InitializeUI()
        {
            stopwatch = new Stopwatch();
            inputField.text = "" + Constants.N_MIN;
            inputField.onValueChanged.AddListener(delegate { OnInputFieldChange(); });
            startButton.onClick.AddListener(delegate () { StartSolver(); });
            loadingBar.gameObject.SetActive(false);
        }

        /// <summary>Determines whether the specified string value is numeric.</summary>
        /// <param name="value">The string value representing N.</param>
        /// <returns>
        ///   <c>true</c> if the specified value is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string value)
        {
            return value.All(char.IsNumber);
        }

        /// <summary>Starts solving the N-Queen problem (if the entered N is valid). This method is called when the button is clicked.</summary>
        private void StartSolver()
        {
            if (!validN) return;
            startButton.interactable = false;
            loadingBar.gameObject.SetActive(true);
            StartCoroutine(StartSolverCoroutine());
            DataHolder.GetInstance().NQueenSolver.Solve(DataHolder.GetInstance().N);
        }

        /// <summary>
        /// A coroutine that starts solving the N-Queen problem and updates the loading bar's progress.
        /// When the solution is ready, the solution is printed in the console (queen sequence) and
        /// the next scene is loaded.
        /// </summary>
        /// <returns>IEnumerator</returns>
        IEnumerator StartSolverCoroutine()
        {
            float progress = 0f;
            stopwatch.Start();
            while (progress < 1f)
            {
                progress = DataHolder.GetInstance().NQueenSolver.CheckCompletion();
                loadingBar.value = progress;
                percentageText.text = (int)(progress * 100f) + "%";
                yield return null;
            }
            stopwatch.Stop();
            DataHolder.GetInstance().solverTime = stopwatch.ElapsedMilliseconds;
            DataHolder.GetInstance().NQueenSolver.PrintSequence();
            StartCoroutine(LoadChessBoardScene());
        }

        /// <summary>A coroutine that loads the chess board scene.</summary>
        /// <returns>IEnumerator</returns>
        IEnumerator LoadChessBoardScene()
        {
            yield return new WaitForSeconds(0.2f);
            SceneManager.LoadScene("ChessBoardScene", LoadSceneMode.Single);
        }

        /// <summary>Determines whether the input for N in the input field is valid. It also changed the input to the maximum allowed N if the input exceeds it.</summary>
        /// <returns><c>true</c> if the input is valid. Otherwise, <c>false</c>.</returns>
        private bool IsValidInput()
        {
            if (IsNumeric(inputField.text))
            {
                try
                {
                    int newN = System.Convert.ToInt32(inputField.text);
                    if (newN < Constants.N_MIN)
                    {
                        return false;
                    }
                    else if (newN > Constants.N_MAX)
                    {
                        newN = Constants.N_MAX;
                        inputField.text = "" + newN;
                    }
                    DataHolder.GetInstance().N = newN;
                    return true;
                }
                catch (FormatException)
                {
                    // Should never occur
                }
            }
            return false;
        }

        /// <summary>Called whenever the input changes. It limits the input to 3 characters and sets the input color to red if it is invalid.</summary>
        private void OnInputFieldChange()
        {
            if (inputField.text.Length > 3)
            {
                inputField.text = inputField.text.Remove(inputField.text.Length - 1);
            }
            if (IsValidInput())
            {
                validN = true;
                inputField.textComponent.color = Color.black;
            }
            else
            {
                validN = false;
                inputField.textComponent.color = Color.red;
            }
        }
    }
}