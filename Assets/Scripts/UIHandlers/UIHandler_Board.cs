﻿using UnityEngine;
using UnityEngine.UI;

namespace NQueen
{
    /// <summary>
    /// The UIHandler_Board class handles the UI for the chess board scene. The UI is only set once when
    /// the scene is loaded. It displays the following information:
    /// <list type="bullet">
    /// <item> N. </item>
    /// <item> The time taken to solve this N-Queen problem. </item>
    /// <item> Controls. </item>
    /// <item> Additional instructions. </item>
    /// </list>
    /// </summary>
    public class UIHandler_Board : MonoBehaviour
    {
        /// <summary>The text in the UI that displays N.</summary>
        public Text text_N;
        /// <summary>The text in the UI that displays the time taken to solve the N-Queen problem.</summary>
        public Text text_Time;

        /// <summary>Start is called before the first frame update (MonoBehaviour method). Initializes the UI for the chess board scene.</summary>
        void Start()
        {
            SetUIInfo();
        }

        /// <summary>Sets the UI for the chess board scene (N and solver time). </summary>
        private void SetUIInfo()
        {
            text_N.text = "N: " + DataHolder.GetInstance().N;
            text_Time.text = "Solver Time: " + DataHolder.GetInstance().solverTime + " ms";
        }
    }
}