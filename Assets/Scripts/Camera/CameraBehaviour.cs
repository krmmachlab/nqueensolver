﻿using UnityEngine;

namespace NQueen
{
    /// <summary>
    /// The CameraBehaviour class handles camera movements through user input.
    /// It allows the user to:
    /// <list type="bullet">
    /// <item>
    /// <term>Move the camera Up/Down/Left/Right</term>
    /// <description>Done using arrow keys or WASD</description>
    /// </item>
    /// <item>
    /// <term>Zoom the camera In/Out</term>
    /// <description>Done using using Q/E</description>
    /// </item>
    /// </list>
    /// </summary>
    public class CameraBehaviour : MonoBehaviour
    {
        /// <summary>The main camera
        /// in the scene.</summary>
        private Camera mainCamera;
        /// <summary>The camera's lower X bound.</summary>
        private float minX;
        /// <summary>The camera's upper X bound.</summary>
        private float maxX;
        /// <summary>The camera's lower Y bound.</summary>
        private float minY;
        /// <summary>The camera's upper Y bound.</summary>
        private float maxY;
        /// <summary>The camera's lower Z bound.</summary>
        private float minZ;
        /// <summary>The camera's upper Z bound.</summary>
        private float maxZ;

        /// <summary>Start is called before the first frame update (MonoBehaviour method). Initializes the camera.</summary>
        void Start()
        {
            InitializeCamera();
        }

        /// <summary>Update is called once per frame (MonoBehaviour method).</summary>
        void Update()
        {
            HandleCameraMovement();
        }

        /// <summary>Initializes the camera's start position and sets camera bounds.</summary>
        private void InitializeCamera()
        {
            mainCamera = Camera.main;
            Vector3 center = new Vector3(DataHolder.GetInstance().N * (0.5f * Constants.TILE_SIZE),
                DataHolder.GetInstance().N * Constants.CAMERA_FACTOR_Y,
                DataHolder.GetInstance().N * (0.5f * Constants.TILE_SIZE));
            mainCamera.transform.position = center;

            SetBounds();
        }

        /// <summary>Sets the camera bounds for X,Y and Z. The camera can't move beyond these bounds.</summary>
        private void SetBounds()
        {
            minX = -Constants.CAMERA_PADDING_XZ;
            maxX = Constants.CAMERA_PADDING_XZ + DataHolder.GetInstance().N * Constants.TILE_SIZE;
            minY = Constants.CAMERA_FACTOR_Y;
            maxY = DataHolder.GetInstance().N * Constants.CAMERA_FACTOR_Y;
            minZ = -Constants.CAMERA_PADDING_XZ;
            maxZ = Constants.CAMERA_PADDING_XZ + DataHolder.GetInstance().N * Constants.TILE_SIZE;
        }

        /// <summary>Handles user input and camera movement accordingly. Arrow Keys / WASD are used
        /// to move the camera up/down/left/right. Q and E are used to zoom the camera
        /// inwards and outwards respectively.</summary>
        private void HandleCameraMovement()
        {
            // Adjust the speed of the camera movement based on the camera's height
            float camStep = Constants.BASE_CAMERA_MOVEMENT * (mainCamera.transform.position.y * 0.1f);
            Vector3 movementVector = new Vector3(0, 0, 0);
            if (Input.GetKey(Constants.CONTROLS_UP1) || Input.GetKey(Constants.CONTROLS_UP2))
            {
                movementVector += new Vector3(0, 0, camStep);
            }
            if (Input.GetKey(Constants.CONTROLS_DOWN1) || Input.GetKey(Constants.CONTROLS_DOWN2))
            {
                movementVector += new Vector3(0, 0, -camStep);
            }
            if (Input.GetKey(Constants.CONTROLS_LEFT1) || Input.GetKey(Constants.CONTROLS_LEFT2))
            {
                movementVector += new Vector3(-camStep, 0, 0);
            }
            if (Input.GetKey(Constants.CONTROLS_RIGHT1) || Input.GetKey(Constants.CONTROLS_RIGHT2))
            {
                movementVector += new Vector3(camStep, 0, 0);
            }
            if (Input.GetKey(Constants.CONTROLS_ZOOMIN))
            {
                movementVector += new Vector3(0, -camStep, 0);
            }
            if (Input.GetKey(Constants.CONTROLS_ZOOMOUT))
            {
                movementVector += new Vector3(0, camStep, 0);
            }

            // Make sure we are within bounds
            Vector3 newCameraPosition = mainCamera.transform.position + movementVector;
            if(newCameraPosition.x < minX || newCameraPosition.x > maxX
                || newCameraPosition.y < minY || newCameraPosition.y > maxY
                || newCameraPosition.z < minZ || newCameraPosition.z > maxZ)
            {
                return;
            }
            mainCamera.transform.position = newCameraPosition;
        }
    }
}